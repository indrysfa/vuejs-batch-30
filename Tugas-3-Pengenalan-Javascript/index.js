// soal 1
// saya senang belajar JAVASCRIPT
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// jawaban soal 1
console.log(pertama.substr(0, 5) + pertama.substr(12, 7) + kedua.substr(0, 8) + kedua.substr(8, 10).toUpperCase())

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var angkaPertama = parseInt(kataPertama);
var angkaKedua =  parseInt(kataKedua);
var angkaKetiga =  parseInt(kataKetiga);
var angkaKeempat =  parseInt(kataKeempat);
// ubahlah variabel diatas ke dalam integer dan lakukan operasi matematika semua variabel agar menghasilkan output 24 (integer).
// jawaban soal 2
var caraKeempat = (angkaPertama % angkaKetiga + angkaKedua) * angkaKeempat
console.log(caraKeempat)

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

// jawaban soal 3
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substr(4, 10); 
var kataKetiga = kalimat.substr(15, 3); 
var kataKeempat = kalimat.substr(19, 5);
var kataKelima = kalimat.substr(25, 7);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// output
// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali