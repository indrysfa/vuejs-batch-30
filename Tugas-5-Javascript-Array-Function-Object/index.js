// soal 1
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// jawaban soal 1
console.log('jawaban soal 1')
console.log(daftarHewan.sort())
console.log('------------------------------------------------------------')
// soal 2
// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 
/* 
    Tulis kode function di sini
*/
// jawaban soal 2
console.log('jawaban soal 2')
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }

function introduce(data) {
    console.log("Nama saya " + data['name'] + ", umur saya " + data['age'] + " tahun, alamat saya di " + data['address'] + ", dan saya punya hobby yaitu " + data['hobby'])
}
var perkenalan = introduce(data)
console.log('------------------------------------------------------------')
// soal 3
// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

// jawaban soal 3
console.log('jawaban soal 3')
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
function hitung_huruf_vokal(hitung_1) {
    var count = hitung_1.match(/[aeiou]/gi).length;
    return count;
}
function hitung_huruf_vokal(hitung_2) {
    var count = hitung_2.match(/[aeiou]/gi).length;
    return count;
}
console.log(hitung_1, hitung_2) // 3 2
console.log('------------------------------------------------------------')

// soal 4
// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan output sebagai berikut.
// console.log( hitung(0) ) // -2
// console.log( hitung(1) ) // 0
// console.log( hitung(2) ) // 2
// console.log( hitung(3) ) // 4
// console.log( hitung(5) ) // 8
// jawaban soal 4
console.log('jawaban soal 4')
var angka = 5
function hitung(angka) {
    for (let angka = -1; angka <= 5; angka++) {
        return angka * 2
        
    }
}
var storage = hitung(0);
console.log(storage) // -2
// console.log( hitung(1) ) // 0
// console.log( hitung(2) ) // 2
// console.log( hitung(3) ) // 4
// console.log( hitung(5) ) // 8