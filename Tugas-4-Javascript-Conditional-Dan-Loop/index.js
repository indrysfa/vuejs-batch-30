// soal 1
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi
var nilai = 45;

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

// jawaban soal 1
console.log('jawaban soal 1')
if (nilai >= 85) {
    console.log('A')
} else if (nilai >= 75 && nilai < 85) {
    console.log('B')
} else if (nilai >= 65 && nilai < 75) {
    console.log('C')
} else if (nilai >= 55 && nilai < 65) {
    console.log('D')
} else if (nilai < 55) {
    console.log('E')
}
console.log('-------------------------------');

// soal 2
var tanggal = 18;
var bulan = 9;
var tahun = 1993;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)

// jawaban soal 2
switch (bulan) {
    case 1:
        bulans = 'January';
        break;
    case 2:
        bulans = 'February';
        break;
    case 3:
        bulans = 'March';
        break;
    case 4:
        bulans = 'April';
        break;
    case 5:
        bulans = 'May';
        break;
    case 6:
        bulans = 'June';
        break;
    case 7:
        bulans = 'July';
        break;
    case 8:
        bulans = 'Agustus';
        break;
    case 9:
        bulans = 'September';
        break;
    case 10:
        bulans = 'October';
        break;
    case 11:
        bulans = 'November';
        break;
    case 12:
        bulans = 'December';
        break;
    default:
        break;
}
console.log('jawaban soal 2')
console.log(tanggal + ' ' + bulans + ' ' + tahun)
console.log('-------------------------------');

// soal 3
// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).
console.log('jawaban soal 3')
console.log('pakai document write')
console.log('-------------------------------');
// jawaban soal 3
// var n = 3;
// for(var n = 0; n <= 7; n++) {
//     for (var j = 0; j < n; j++) {
//         document.write("#")
//     }
//     document.write("<br>")
// } 

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// var m = 0
var kata = 9
var string = [' - I love programming', ' - I love Javascript', ' - I love VueJS']
var satu = '- I love programming'
var dua = '- I love Javascript'
var tiga = '- I love VueJS'
var empat = '='
console.log('jawaban soal 4')
for (var m = 1; m % kata; m++) {
    console.log(m++ + string[0])
    for (let m = 2; m % kata; m++) {
        console.log(m + string[1])
        for (let m = 3; m % kata; m++) {
            console.log(m + string[2])
            for (let m = 9; m % kata; m++) {
                console.log(empat * 2)
            }
        }
    }
}