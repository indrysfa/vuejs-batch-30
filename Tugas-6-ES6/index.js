// soal 1
// buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini

// jawaban soal 1
let hitungLuas = () => {
    let p = 12
    let l = 6
    hitungLuas = p * l
    console.log(hitungLuas)
}
// panggil Function
hitungLuas()

const keliling = () => {
    const p = 12
    const l = 6
    hitungKeliling = (2 * p) + (2 * l)
    console.log(hitungKeliling)
}
// panggil Function
keliling()

// soal 2
// Ubahlah code di bawah ke dalam arrow function dan object literal es6 yang lebih sederhana
const newFunction1 = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
    }
  }
}
 
//Driver Code 
// newFunction1("William", "Imoh").fullName() 

// jawaban soal 2
const keliling1 = (firstName, lastName) => {
    const firstName: "William",
    const lastName: "Imoh",
    const fullName: function(){
      console.log(firstName + " " + lastName)
    }
    // const john = {firstName , lastName}
}
// panggil Function
keliling1('William' , 'Imoh')

// soal 3
// Diberikan sebuah objek sebagai berikut:
// const newObject = {
//   firstName: "Muhammad",
//   lastName: "Iqbal Mubarok",
//   address: "Jalan Ranamanyar",
//   hobby: "playing football",
// }
// dalam ES5 untuk mendapatkan semua nilai dari object tersebut kita harus tampung satu per satu:
// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const address = newObject.address;
// const hobby = newObject.hobby;
// Gunakan metode destructuring dalam ES6 untuk mendapatkan semua nilai dalam object dengan lebih singkat (1 line saja)
// // Driver code
// console.log(firstName, lastName, address, hobby)